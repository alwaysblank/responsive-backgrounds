// Adjustes sizes for background images based on screen size
// Note: requires responsive_background() PHP function.
(function($) {
    $( document ).ready(function() {
        function responsiveBG() {
          $('[data-bg-img]').each(function() {
                var self = $(this);
                var images = self.data('bg-img').split(' ');
                $.each(images, function(i, image) {
                    var imageArr = image.split(';');
                    if($( window ).width() > imageArr[0]) {
                        self.css('background-image', 'url(' + imageArr[1] + ')');
                    }
                });
            });
        }

        // run when we load
        responsiveBG(); 

        // run again when we re-size
        var resizeTimer;
        $(window).resize(function(e) {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function() {
            responsiveBG();                    
          }, 250);
        });
    });
})(jQuery);