<?php
/**
 * Plugin Name: Responsive BG Images
 * Plugin URI: http://murmurcreative.com
 * Description: This implements an extensible solution for responsive background images.
 * Version: 1.0.1
 * Author: Ben Bateman
 * Author URI: http://murmurcreative.com
 */

add_action( 'wp_enqueue_scripts', 'responsive_bg_assets' );

function responsive_bg_assets() {
    wp_enqueue_script( 'my-script', plugin_dir_url( __FILE__ ) . '/js/script.js', array( 'jquery' ), '1.0', true  );
}


function responsive_background($image_id, $default = null, $sizes = [
    'sm' => [
      'name' => 'thumbnail',
      'width' => 480
      ], 
    'md' => [
      'name' => 'medium',
      'width' => 960
      ],
    'lg' => [
      'name' => 'large',
      'width' => 1200
      ],
    'xl' => [
      'name' => 'x-large',
      'width' => 2560
      ]
    ]) 
  {
  // This returns three different image sizes, the
  // background-image style property, and the data-bg-img
  // attributes necessary for switching.

  // If there isn't an image, use the default.
  if(!wp_get_attachment_image_src( $image_id)):
    if(class_exists('acf_options_page_plugin')){
        $image_id = get_field('default_bg','option');
    }
    else {
        return 'no image';
    }
  endif;

  $image = [];
  $image['sizes'] = [];
  $previous = 0;
  foreach ($sizes as $size => $data) {
    $image['sizes'][$size] = sprintf('%3$s;%2$s', $size, wp_get_attachment_image_src( $image_id, $data['name'])[0], $previous);
      $previous = $data['width'];
  }
  $return = join($image['sizes'], ' ');
  $return = " data-bg-img='$return'";
  if($default == null):
    reset($sizes);
    $pos = $sizes[key($sizes)]['name'];
  else: 
    $pos = $default;
  endif;
  $return = sprintf('style="background-image:url(%s)" %s', wp_get_attachment_image_src( $image_id, $pos)[0], $return );
  unset($image);
  unset($sizes);
  return $return;
}